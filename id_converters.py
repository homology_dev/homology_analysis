#from domain_work import *
#from get_ids_from_msa_files import *
#from id_converters import *
#from interaction_search import *
#from script_running import *
#from binder import *
import os
import sqlite3

##Converts Uniprot ID like P14778 to list of it's domains with it's occurrence [IPR000157, ...]
##Using InterPro Domain Database

def get_domain_id_list (uniprot_id, conn):

	domain_id = list()
	c = conn.cursor()


	row1 = c.execute('SELECT item_id_b FROM protein_actions WHERE item_id_a=?', (uniprot_id,))

	data1 = []
	data2 = []
	data3 = []

	for i in row1:
	        data1.append(i[0])

	row2 = c.execute('SELECT start FROM protein_actions WHERE item_id_a=?', (uniprot_id,))

	for i in row2:
	        data2.append(i[0])

	row3 = c.execute('SELECT end FROM protein_actions WHERE item_id_a=?', (uniprot_id,))

	for i in row3:
	        data3.append(i[0])

	for i in range(len(data1)):
		domain_id.append(data1[i] + ' ' + data2[i] + ' ' + str(int(data3[i])))

	for row in c.execute('SELECT item_id_a, start, end FROM protein_actions WHERE item_id_b=?', (uniprot_id,)):
		domain_id.append(row[0])
		
        return list (domain_id)


##Converts Uniprot ID like P14778 to list of it's domains with it's occurrence [IPR000157, ...]
##Using InterPro Domain Database

def get_domain_id (uniprot_id, conn):

	domain_id = set()
	c = conn.cursor()

	row1 = c.execute('SELECT item_id_b FROM protein_actions WHERE item_id_a=?', (uniprot_id,))

	data1 = []
	data2 = []
	data3 = []

	for i in row1:
	        data1.append(i[0])

	row2 = c.execute('SELECT start FROM protein_actions WHERE item_id_a=?', (uniprot_id,))

	for i in row2:
	        data2.append(i[0])

	row3 = c.execute('SELECT end FROM protein_actions WHERE item_id_a=?', (uniprot_id,))

	for i in row3:
	        data3.append(i[0])

	for i in range(len(data1)):
		domain_id.update([data1[i]])


	for row in c.execute('SELECT item_id_a, start, end FROM protein_actions WHERE item_id_b=?', (uniprot_id,)):
		domain_id.update([row[0]])

        return list (domain_id)


##Coverts Uniprot ID like Q00959 into it's gene ID Grin2a and vice versa
##Using Uniprot database

def get_gene_id (uniprot, conn):
	res = []
	c = conn.cursor()

	for row in c.execute('SELECT item_id_b FROM protein_actions WHERE item_id_a=?', (uniprot,)):
		res.append(row[0])	

	for row in c.execute('SELECT item_id_a FROM protein_actions WHERE item_id_b=?', (uniprot,)):
		res.append(row[0])
	if (len(res) != 0):
		if (len(res) == 1):
                	return str(res[0])
		else:
			return res
	else:
		return uniprot


##Converts PDB chain ID like 1g0y_R into it's Uniprot ID P14778 and vice versa
##Using pdb_chain_uniprot database

def uniprot_pdb_chain_converter (gene, conn):

	res = []
	c = conn.cursor()

	for row in c.execute('SELECT item_id_b FROM protein_actions WHERE item_id_a=?', (gene,)):
		res.append(row[0])

	for row in c.execute('SELECT item_id_a FROM protein_actions WHERE item_id_b=?', (gene,)):
		res.append(row[0])

        if (len(res) == 0):
                return gene
        else:
                if (len(res) == 1):
                        return res[0]
                else:
                        return res


##Converts Uniprot ID like P14778 into it's uniprot.org Entry name IL1R1_HUMAN and vice versa
##Using pdbtosp database

def get_uniprot_id (gene, conn):
	res = []
	c = conn.cursor()

	for row in c.execute('SELECT item_id_b FROM protein_actions WHERE item_id_a=?', (gene,)):
		res.append(row[0])

	for row in c.execute('SELECT item_id_a FROM protein_actions WHERE item_id_b=?', (gene,)):
		res.append(row[0])

	if (len(res) != 0):
		return res[0]
	else:
		return gene


##Converts list of Entry names like [IL1R1_HUMAN, ...] into list of its pdb chain ID's [1g0y_R, ...] 

def uniprot_to_pdb (uniprot_ids, uni_conn, chain_conn):

	pdb_ids = set()
	buf_ids = set()

	uni_c = uni_conn.cursor()
	chain_c = chain_conn.cursor()
	
        for x in uniprot_ids:
		res = []
		for row in uni_c.execute('SELECT item_id_b FROM protein_actions WHERE item_id_a=?', (x,)):
	                res.append(row[0])

	        for row in uni_c.execute('SELECT item_id_a FROM protein_actions WHERE item_id_b=?', (x,)):
	                res.append(row[0])

	        if (len(res) != 0):
			buf_ids.update([res[0]])

        for x in buf_ids:
		res = []
		for row in chain_c.execute('SELECT item_id_b FROM protein_actions WHERE item_id_a=?', (x,)):
                	res.append(row[0])

        	for row in chain_c.execute('SELECT item_id_a FROM protein_actions WHERE item_id_b=?', (x,)):
        	        res.append(row[0])
        	if (len(res) == 0):
                	res = [x]
        	else:
                	if (len(res) == 1):
                        	res = [res[0]]
              	
		for i in res:
			if (i != x):
				pdb_ids.update([i])		


	result = (list(pdb_ids))	
	return result


## Converts String ID into set of Uniprot ID's

def string_to_uniprot (string, conn):

	c = conn.cursor()

	uniprot = set()

	for row in c.execute('SELECT item_id_b FROM protein_actions WHERE item_id_a=?', (string,)):
		uniprot.update([row[0]])   

	for row in c.execute('SELECT item_id_a FROM protein_actions WHERE item_id_b=?', (string,)):
		uniprot.update([row[0]])

	return uniprot


##Gets list of domains which are in sequence in file 'gene'

def get_domains_list (gene, interpro):

	## Get domains ids with positions

	domain_id = list()
	os.system (interpro + '/interproscan.sh -i ' + gene + ' -iprlookup -o ../tmp/interpro_' + gene + ' -f TSV')
        tmp = open ('../tmp/interpro_' + gene, 'r').readlines()
	for i in tmp:
		if len(i.split('\t')) == 13:
			domain_id.append([i.split('\t')[11], i.split('\t')[6], i.split('\t')[7]])
        return domain_id

