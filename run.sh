#!/bin/bash
HH_INSTALL_DIR=$(pwd)/../HH_Install
export HHLIB=${HH_INSTALL_DIR}
export PATH=${PATH}:${HH_INSTALL_DIR}/bin/
python run.py "$@"
