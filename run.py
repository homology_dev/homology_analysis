import os
import argparse
import numpy as np
import pandas as pd
from Bio import SeqIO
from multiprocessing import Pool
from Bio import SeqIO
import ConfigParser
from Bio.PDB import *
from domain_work import *
from get_ids_from_msa_files import *
from id_converters import *
from interaction_search import *
from script_running import *
from sql_connections import *

def ConfigSectionMap(section):
    dict1 = {}
    options = Config.options(section)
    for option in options:
        try:
            dict1[option] = Config.get(section, option)
            if dict1[option] == -1:
                DebugPrint("skip: %s" % option)
        except:
            print("exception on %s!" % option)
            dict1[option] = None
    return dict1

Config = ConfigParser.ConfigParser()
Config.read("./config")

pdbtosp = ConfigSectionMap("PATHS")['pdbtosp']#x[0].split('|')[1].split('\n')[0]
biogrid = ConfigSectionMap("PATHS")['biogrid']#x[1].split('|')[1].split('\n')[0]
uniprot = ConfigSectionMap("PATHS")['uniprot']#x[2].split('|')[1].split('\n')[0]
string  = ConfigSectionMap("PATHS")['string']#x[3].split('|')[1].split('\n')[0]
uniprot20 = ConfigSectionMap("PATHS")['uniprot20']#x[4].split('|')[1].split('\n')[0]
pdb100 = ConfigSectionMap("PATHS")['pdb100']#x[5].split('|')[1].split('\n')[0]
full_uniprot = ConfigSectionMap("PATHS")['full_uniprot']#x[6].split('|')[1].split('\n')[0]
all_knowledge = ConfigSectionMap("PATHS")['all_knowledge']#x[7].split('|')[1].split('\n')[0]
interpro = ConfigSectionMap("PATHS")['interpro']

seq_identity = int(ConfigSectionMap("hhblits options")['seq_identity'])
cpu = int(ConfigSectionMap("hhblits options")['cpu'])
overlap = int (ConfigSectionMap("hhblits options")['overlap'])

## Function which tells do alignment sequence has block only with length more than 'seq_len' or not.
## Blocks are separated with '-' symbol
## seq - sequence from hhblits output file
## output - 1 if sequence has block only with length more than 'seq_len' letters, 0 - otherwise 

def seq_test (seq, seq_len):

	## Right sequence - if each MSA block contains >= 60 letters

        count = 0
        flag = 0
        for i in seq:
                if (i == '-'):
                        if flag:
                                if (count < seq_len) & (count > 0):
                                        return 0
                        count = 0
                else:
                        flag = 1
                        count += 1
        if ((count > 0) & (count < seq_len)):
                return 0
        else:
                return 1


def main ():

	#print "START"

	## Input sequences as a command line args
	
	parser = argparse.ArgumentParser (description='Finding protein interactions')
	parser.add_argument ('protein_A', metavar='prot_A',  help='file with sequence A')
	parser.add_argument ('protein_B', metavar='prot_B',  help='file with sequence B')
        parser.add_argument ('fout_biogrid', metavar='pdb_out',  help='output file for pdb interactions')
        parser.add_argument ('fout_string', metavar='uniprot_out',  help='output file for uniprot interactions')
	parser.add_argument ('fout_complex', metavar='complex_out',  help='output file for complexes')
	parser.add_argument ('fout_align_prot_A', metavar='align_out',  help='output file for multiple sequence alignment for sequence A')
	parser.add_argument ('fout_align_prot_B', metavar='align_out',  help='output file for multiple sequence alignment for sequence B')
	parser.add_argument ('fout_domain_A', metavar='domain_A',  help='output file for protein_A domain')
	parser.add_argument ('fout_domain_B', metavar='domain_B',  help='output file for protein_B domain')
	parser.add_argument ('fout_distance', metavar='distance_out',  help='output file for domains_distances')		

        args = parser.parse_args ()

	## Open connections to the databases
	
	conn_all_go_knowledge, conn_chain_uniprot, conn_protein2ipr, conn_full_uniprot, conn_pdbtosp, conn_uniprot_to_gene = open_connections ()
	
	## Find similar proteins to given A and B proteins
	## Proteins will be presented as list of sequences in fasta format sepatately according to pdb and uniprot databases
	## hhblits options description can be found in script_running file 

	run_hhblits (pdb100, cpu, seq_identity, args.protein_A, 'pdb_' + args.fout_align_prot_A)
	run_hhblits (uniprot20, cpu, seq_identity, args.protein_A, 'uni_' + args.fout_align_prot_A)
	
	run_hhblits (pdb100, cpu, seq_identity, args.protein_B, 'pdb_' + args.fout_align_prot_B)
	run_hhblits (uniprot20, cpu, seq_identity, args.protein_B, 'uni_' + args.fout_align_prot_B)

	## Get pdb ID's and ID's of the prime proteins from hhblits outputs
	## Prime ID's will be use only for zlab testing
	
	prime_A, pdb_ids_A = get_pdb_ids ('pdb_' + args.fout_align_prot_A)
	prime_B, pdb_ids_B = get_pdb_ids ('pdb_' + args.fout_align_prot_B)

	## Transform pdb ID's into string and biogrid ID's to use them to find interactions in BIOGRID and String databases 
	
	biogrid_ids_A = get_biogrid_chain_ids ('pdb_' + args.fout_align_prot_A, 'uni_' + args.fout_align_prot_A, overlap, conn_chain_uniprot, conn_pdbtosp)
	string_ids_A = get_string_ids_sql ('pdb_' + args.fout_align_prot_A, 'uni_' + args.fout_align_prot_A, overlap, conn_chain_uniprot, conn_pdbtosp, conn_full_uniprot)
	biogrid_ids_B = get_biogrid_chain_ids ('pdb_' + args.fout_align_prot_B, 'uni_' + args.fout_align_prot_B, overlap, conn_chain_uniprot, conn_pdbtosp)
        string_ids_B = get_string_ids_sql ('pdb_' + args.fout_align_prot_B, 'uni_' + args.fout_align_prot_B, overlap, conn_chain_uniprot, conn_pdbtosp, conn_full_uniprot)

	## Keep A and B biogrid ID's for future matching 
	## e.g. after interaction finding one string ID can be transform into many biogrid ID's 
	## So, we should leave only those ID's which we have in alignment files 
 
	to_keep_a = biogrid_ids_A
	to_keep_b = biogrid_ids_B

	## Find interactions
	
	if (len (biogrid_ids_A) > len (biogrid_ids_B)):
		biogrid_interactions = find_biogrid_interactions (biogrid_ids_B, biogrid_ids_A, cpu, biogrid, conn_pdbtosp, conn_uniprot_to_gene)
		string_interactions = find_string_interactions (string_ids_B, string_ids_A, cpu, string)
	else:
		biogrid_interactions = find_biogrid_interactions (biogrid_ids_A, biogrid_ids_B, cpu, biogrid, conn_pdbtosp, conn_uniprot_to_gene)
                string_interactions = find_string_interactions (string_ids_A, string_ids_B, cpu, string)

	pdb_interactions = find_pdb_id_sql (biogrid_interactions, 'B', conn_full_uniprot, conn_pdbtosp, conn_chain_uniprot) + find_pdb_id_sql (string_interactions, 'S', conn_full_uniprot, conn_pdbtosp, conn_chain_uniprot)
	uniprot_interactions = biogrid_interactions + find_uniprot_id_sql (string_interactions,  conn_full_uniprot)

	## Write down the results

	output = open (args.fout_biogrid, 'w')
	for x in pdb_interactions:
		output.write(str(x))

	output = open (args.fout_string, 'w')
	for x in uniprot_interactions:
                x_a = x.split(' ')[0]
		x_b = x.split(' ')[1].split('\n')[0]
		flag = 1
		if ((x_a in to_keep_a) & (x_b in to_keep_b)):
			output.write(x_a + ' ' + x_b + '\n')
		if ((x_a in to_keep_b) & (x_b in to_keep_a)):
			output.write(x_b + ' ' + x_a + '\n')
	
	## Finding complexes
	
	complexes = []
	test = list()
	for x in pdb_interactions:
		if (x.split(' ')[0].split('_')[0] == x.split(' ')[1].split('\n')[0].split('_')[0]):
			complexes.append(x.split(' ')[0] + ':' + x.split(' ')[1].split('\n')[0].split('_')[1] + '\n')
			test.append([(x.split(' ')[0] + ':' + x.split(' ')[1].split('\n')[0].split('_')[1] + '\n')])


	output = open (args.fout_complex, 'w')
	for x in test:
		for y in x:
			output.write(str(y))
	output.close()

	find_domains (args.fout_string, args.fout_domain_A, args.fout_domain_B, conn_pdbtosp, conn_protein2ipr)	

	output_binder (interpro, prime_A, args.fout_distance + '_receptor', args.protein_A, args.fout_domain_A)
	output_binder (interpro, prime_B, args.fout_distance + '_ligand', args.protein_B, args.fout_domain_B)
	'''	
	domains_A_co, domains_B_co, domains_co = find_interacting_domains (interpro, args.fout_align_prot_A, args.fout_align_prot_B, args.fout_complex, args.protein_A + , args.protein_B + , pdb_ids_A, pdb_ids_B, args.fout_distance)	
	domains_A_contra, domains_B_contra, domains_contra = find_interacting_domains (interpro, args.fout_align_prot_B, args.fout_align_prot_A, args.fout_complex, args.protein_B + , args.protein_A + , pdb_ids_B, pdb_ids_A, args.fout_distance)
	dist_A, dist_B = prime_domains (args.fout_domain_A, args.fout_domain_B, domains_co, domains_contra, args.fout_distance)
	output_binder1 (interpro, prime_A, prime_B, args.fout_distance, domains_A_co, domains_A_contra, dist_A, dist_B, args.protein_A + , args.protein_B + , args.fout_domain_A, args.fout_domain_B)	
	'''
if __name__ == '__main__':
        main()

