#!/bin/bash
set -euo pipefail

cd ../
mkdir -p tmp id_mapping interaction_data seq_data
cd homology_analysis

curl -O ftp://ftp.ebi.ac.uk/pub/databases/interpro/protein2ipr.dat.gz
gunzip protein2ipr.dat.gz
echo "Processing sqlite database, may take some time..."
python make_protein2ipr_db.py
rm protein2ipr.dat

cd ../id_mapping/
curl -O http://string-db.org/mapping_files/uniprot_mappings/full_uniprot_2_string.04_2015.tsv.gz
gunzip full_uniprot_2_string.04_2015.tsv.gz
cd ../homology_analysis
echo "Processing sqlite database, may take some time..."
python make_full_uni_db.py
rm ../id_mapping/full_uniprot_2_string.04_2015.tsv

cd ../id_mapping/
curl -O ftp://ftp.ebi.ac.uk/pub/databases/msd/sifts/flatfiles/tsv/pdb_chain_go.tsv.gz
gunzip pdb_chain_go.tsv
cd ../homology_analysis
echo "Processing sqlite database, may take some time..."
python make_pdb_chain_go.py
rm ../id_mapping/pdb_chain_go.tsv

cd ../id_mapping/
curl -O ftp://ftp.ebi.ac.uk/pub/databases/msd/sifts/flatfiles/tsv/pdb_chain_uniprot.tsv.gz
gunzip pdb_chain_uniprot.tsv.gz
cd ../homology_analysis
echo "Processing sqlite database, may take some time..."
python make_pdb_chain_uniprot_db.py
rm ../id_mapping/pdb_chain_uniprot.tsv

cd ../id_mapping/
curl -O http://www.uniprot.org/docs/pdbtosp.txt
tail -n +24 pdbtosp.txt  | head -n -5 > _pdbtosp.txt
cd ../homology_analysis
echo "Processing sqlite database, may take some time..."
python make_pdbtosp_db.py
rm ../id_mapping/pdbtosp.txt
rm ../id_mapping/_pdbtosp.txt

cd ../id_mapping/
curl -O ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/idmapping/idmapping.dat.gz
gunzip idmapping.dat.gz
cd ../homology_analysis
echo "Processing sqlite database, may take some time..."
python make_uniprot_to_gene_db.py
rm ../id_mapping/idmapping.dat
cd ..


cd interaction_data
curl -O http://string-db.org/download/protein.actions.v10.5.txt.gz
gunzip protein.actions.v10.5.txt.gz
cd ../homology_analysis
echo "Processing sqlite database, may take some time..."
python make_sqlite_db_paired.py
cd ../interaction_data
rm protein.actions.v10.5.txt

curl -O https://thebiogrid.org/downloads/archives/Release%20Archive/BIOGRID-3.4.148/BIOGRID-ALL-3.4.148.mitab.zip 
unzip BIOGRID-ALL-3.4.148.mitab.zip

curl -O ftp://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot_varsplic.fasta.gz
gunzip uniprot_sprot_varsplic.fasta.gz
cd ..

cd seq_data
curl -O http://wwwuser.gwdg.de/~compbiol/data/hhsuite/databases/hhsuite_dbs/uniprot20_2016_02.tgz
tar -x -z --strip-components=1 -f uniprot20_2016_02.tgz
curl -O http://japetus.bu.edu/~drhall/pdb100.tgz
tar xzf pdb100.tgz
cd ..

HH_INSTALL_DIR=$(pwd)/HH_Install
mkdir -p ${HH_INSTALL_DIR}
git clone https://github.com/soedinglab/hh-suite.git
cd hh-suite
git checkout 5ca314133a3c99e97af586393d57d6de80f4e073
git submodule init
git submodule update
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -G "Unix Makefiles" -DCMAKE_INSTALL_PREFIX=${HH_INSTALL_DIR} ..
make
make install
cd ../..

export HHLIB=${HH_INSTALL_DIR}
export PATH=${PATH}:${HH_INSTALL_DIR}/bin/

cd homology_analysis
curl -O ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/5.21-60.0/interproscan-5.21-60.0-64-bit.tar.gz
tar xzf interproscan-5.21-60.0-64-bit.tar.gz

gcc finder.c -o finder.o


