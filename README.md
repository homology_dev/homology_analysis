# Homology analysis

## Installing

Installation requires around 160GB of free disk space and 8GB of RAM. The following guide was tested on Ubuntu 16.04, but should work on other versions.

First, install dependencies from standard system repositories:
```
sudo apt-get install gcc cmake git openjdk-8-jre python-pandas python-numpy python-biopython
```

Then, from project directory run installation script:
```
./install.sh
```

## Usage

Currently, the program only works if started from project directory (i.e. you should be in the same directory where ./run.sh is).

To run program on example data:
```
./run.sh Q16620.fasta P34130.fasta pdb_out.txt uniprot_out.txt complex_out.txt align_out.txt align_out2.txt domain_A.txt domain_B.txt distance_out.txt
```

This script requires 10 arguments, which are:

* prot_A: file with sequence A in fasta format
* prot_B: file with sequence B in fasta format
* pdb_out: output file for pdb interactions
* uniprot_out: output file for uniprot interactions
* complex_out: output file for complexes
* align_out_A: output file for multiple sequence alignment for sequence A
* align_out_B: output file for multiple sequence alignment for sequence B
* domain_A: output file for protein_A domain
* domain_B: output file for protein_B domain
* distance_out: output file for domains_distances
