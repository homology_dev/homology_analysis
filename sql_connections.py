import sqlite3
import argparse


def open_connections ():
	conn_all_go_knowledge = sqlite3.connect('all_go_knowledge_full_paired.db')
	conn_chain_uniprot = sqlite3.connect('pdb_chain_uniprot_paired.db')
	conn_protein2ipr = sqlite3.connect('protein2ipr-paired.db')
	conn_full_uniprot = sqlite3.connect('full_uniprot_2_string.04_2015_paired.db')
	conn_pdbtosp = sqlite3.connect('pdbtosp_paired.db')
	conn_uniprot_to_gene = sqlite3.connect('uniprot_to_gene_2.db')
	
	return conn_all_go_knowledge, conn_chain_uniprot, conn_protein2ipr, conn_full_uniprot, conn_pdbtosp, conn_uniprot_to_gene


